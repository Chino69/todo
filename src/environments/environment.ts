// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCRUq0CFwnJfAT11i2ezWkRpqmY7GNKj9k",
  authDomain: "ya-todo-sirve.firebaseapp.com",
  databaseURL: "https://ya-todo-sirve.firebaseio.com",
  projectId: "ya-todo-sirve",
  storageBucket: "",
  messagingSenderId: "985978214241",
  appId: "1:985978214241:web:5bcf8f7734d6c5cf"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
