import { Component, OnInit } from '@angular/core';
import { Facebook } from "@ionic-native/facebook/ngx"
import { AngularFireAuth} from '@angular/fire/auth'
import {Router} from '@angular/router';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import * as firebase from 'firebase';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private facebook: Facebook, private gplus: GooglePlus, public router: Router, private afAuth: AngularFireAuth) {
    this.afAuth.authState.subscribe(
      async user => {
        if(user !== null){
          this.router.navigate(['/home'])
        }else{

        }

      } 

    )
   }
   facebookLogin(): Promise<any> {
    return this.facebook.login(['email', 'public_profile'])
      .then( response => {
        const facebookCredential = firebase.auth.FacebookAuthProvider
          .credential(response.authResponse.accessToken);    
  firebase.auth().signInWithCredential(facebookCredential)
    .then( success => {
       console.log("Firebase success: " + JSON.stringify(success));
       
     });
  }).catch((error) => {
      console.log(error);
      
     });
  }
  async nativeGoogleLogin(): Promise<any> {
    try {
  
      const gplusUser = await this.gplus.login({
        'webClientId': '985978214241-2luop3vk0gklt8a75pefpe572sbka1uu.apps.googleusercontent.com',
        'offline': false,
      })
  
      return await this.afAuth.auth.signInWithCredential(firebase.auth.GoogleAuthProvider.credential(gplusUser.idToken))
   
    } catch(err) {
      console.log(err)
    }
    
    
  }


  ngOnInit() {
  }

}
