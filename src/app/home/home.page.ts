import { Component } from '@angular/core';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { File } from '@ionic-native/file/ngx';
import {DomSanitizer} from '@angular/platform-browser'
import { Base64 } from '@ionic-native/base64/ngx';
import * as firebase from 'firebase'
import {AngularFireAuth} from '@angular/fire/auth';
import {Router} from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
image: any;

  constructor(public imagePicker: ImagePicker, public router:Router, public afAuth: AngularFireAuth, public file: File, private base64: Base64, public sanitizer: DomSanitizer) {}
  
  async openImagePicker(){
    await this.imagePicker.hasReadPermission()
    .then(async (result) => {
      if(result == false){
        // no callbacks required as                    this opens a popup which returns async
        this.imagePicker.requestReadPermission();
      }
      else if(result == true){
      console.log("Ya tiene permisos Eso!");
      await this.imagePicker.getPictures({
       maximumImagesCount: 1
      }).then(
      async (results) => {
        for (var i = 0; i < results.length; i++) {
          const aux = this;

          const element = results[i];
          //await  this.uploadImageToFirebase(results[i]);
          let filePath: string = '';
          this.base64.encodeFile(element).then( async (base64File: string) => {
            console.log(base64File);
            this.image = this.sanitizer.bypassSecurityTrustUrl(base64File);
            console.log(this.image);
            console.log('Despues sigue firebase');
            firebase.database().ref('pictures/').push({image: aux.image}).then(sn => {
              console.log(sn.key, 'CARAJOOOOOOOOOOOO')
            })
           console.log('NO FUE FIREBASE');
          }, (err) => {
            console.log('ERROR PRIMERO')
            
          });  
          console.log('no se imagenes')
        console.log(results)
        
        }
               }, (err) => console.log('ERROR SEGUNDO', )
      );
      }
    }, (err) => {
      console.log(' ERROR, ultimo')
    });
  }
  logOut(){
    this.afAuth.auth.signOut();
    this.router.navigate(['/login'])
  }
  
}